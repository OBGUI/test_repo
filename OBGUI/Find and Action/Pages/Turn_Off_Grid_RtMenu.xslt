﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="java ns"     xmlns="http://www.seagullsoftware.com/schemas/legasuite/panel"
    xmlns:ns="http://www.seagullsoftware.com/schemas/legasuite/panel"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:java="http://xml.apache.org/xslt/java">
<!-- CHANGE SEQUENCE (Grid)                                        -->
<!-- Search criteria:                                              -->
<!--   Find property Right-mouse enabled with value "Enabled".     -->
<!--   Find property Context menu with value "OPTIONS (MENUHOLD)". -->
<!--   Find property Right mouse menu with value "Custom".         -->
<!-- Actions:                                                      -->
<!--   Set property Right-mouse enabled to Disabled.               -->
<!--   Set property Horizontal alignment to Default.               -->
<!--   Set property Context menu to .                              -->
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes" />
<xsl:template match="ns:ajax_grid">
  <xsl:choose>
      <xsl:when test="(count(./ns:ajax_opts_common/ns:ajax_enable_right_mouse_menu)=1) and ((./ns:right_mouse_popup_panel_name/text()[normalize-space() = normalize-space('MENUHOLD')]) and (./ns:right_mouse_menu_name/text()[normalize-space() = normalize-space('OPTIONS')])) and (((count(./ns:ajax_opts_common) = 0) or (count(./ns:ajax_opts_common/ns:ajax_native_context_menu)=0))) and (((count(./ns:ajax_opts_common) = 0) or (count(./ns:ajax_opts_common/ns:ajax_inherit_context_menu)=0)))">
        <xsl:copy>
           <xsl:apply-templates select="*[not(self::ns:ajax_opts_common) and not(self::ns:ajax_align_x) and not(self::ns:right_mouse_menu_name) and not(self::ns:right_mouse_popup_panel_name)]"/>
           <ajax_opts_common>
               <xsl:copy-of select="./ns:ajax_opts_common/*[name()!='ajax_enable_right_mouse_menu']"/>
           </ajax_opts_common>
           <ajax_align_x>0</ajax_align_x>
           <right_mouse_popup_panel_name></right_mouse_popup_panel_name>
           <right_mouse_menu_name></right_mouse_menu_name>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
           <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Copy an ordinary node or attribute-->
<xsl:template match="@*|node()">
  <xsl:copy>
     <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>
</xsl:stylesheet>
