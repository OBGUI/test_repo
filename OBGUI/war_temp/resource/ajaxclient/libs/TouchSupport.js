/**
 * Created with IntelliJ IDEA.
 * User: ndyumin
 * Date: 02.04.15
 * Time: 17:28
 */

function createTouchEvent(el, opts) {
    var touch = document.createTouch(window, el, opts.id, opts.clientX, opts.clientY, opts.screenX, opts.screenY),
        touches = document.createTouchList(touch),
        targetTouches = document.createTouchList(touch),
        changedTouches = document.createTouchList(touch),
        event = document.createEvent("TouchEvent");

    event.initTouchEvent(opts.type, true, true, window, null, opts.clientX, opts.clientY, opts.screenX, opts.screenY, false, false, false, false,
        touches, targetTouches, changedTouches, 1, 0);
    event.$$customEvent = true;
    return event;
}

function isQxScrollBar(element) {
    var hops = 3,
        result = false;
    while (element && hops) {
        if (element.getAttribute("qxclass") === "qx.ui.core.scroll.ScrollBar") {
            result = true;
            break;
        }
        hops -= 1;
        element = element.parentElement;
    }
    return result;
}

function isInputText(element) {
    var result = false;
    if ((element.nodeName === "INPUT") && (element.getAttribute("type") === "text")) {
        result = true;
    }
    return result;
}

function onTouchMoveEvent(e) {
    if (!e.$$customEvent) {
        var touch = (e.touches && e.touches[0]) || e.changedTouches[0],
            element = document.elementFromPoint(touch.clientX, touch.clientY),
            event;
        if (e.touches.length > 1) {
            e.stopImmediatePropagation();
            if (document.activeElement) {
                document.activeElement.blur();
            }
        } else if ((e.touches.length === 1) && !isQxScrollBar(element) && !isInputText(element)) {
            e.stopImmediatePropagation();
            event = createTouchEvent(element, {
                type: "touchmove",
                id: touch.identifier,
                clientX: touch.clientX,
                clientY: touch.clientY,
                screenX: touch.screenX,
                screenY: touch.screenY
            });
            element.dispatchEvent(event);
        }
    }
}

function isUserScalable() {
    var metas = document.getElementsByTagName("meta"),
        mLen = metas.length,
        result = true,
        i;
    for (i = 0; i < mLen; i += 1) {
        if ((metas[i].name.toLowerCase() === "viewport") && (/user-scalable=no/i.test(metas[i].content.toLowerCase()))) {
            result = false;
        }
    }
    return result;
}

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && isUserScalable()) {
    console.info("Touch support enabled");
    window.addEventListener("touchmove", onTouchMoveEvent, true);
    document.body.addEventListener("onbeforeunload", function() {
        window.removeEventListener("touchmove", onTouchMoveEvent);
    });
}