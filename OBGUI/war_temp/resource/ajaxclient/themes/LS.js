/* ************************************************************************

 qooxdoo - the new era of web development

 http://qooxdoo.org

 Copyright:
 2004-2011 1&1 Internet AG, Germany, http://www.1und1.de

 License:
 LGPL: http://www.gnu.org/licenses/lgpl.html
 EPL: http://www.eclipse.org/org/documents/epl-v10.php
 See the LICENSE file in the project's top-level directory for details.

 Authors:
 * Martin Wittemann (martinwittemann)

 ************************************************************************ */
/**
 * Legasuite Theme
 */
qx.Theme.define("qx.theme.LS", {
    title: "LS",

    meta: {
        color: qx.theme.ls.Color,
        decoration: qx.theme.ls.Decoration,
        font: qx.theme.ls.Font,
        appearance: qx.theme.ls.Appearance,
        icon: qx.theme.icon.Tango
    }
});