/**
 * Contemporary Theme
 */
qx.Theme.define("qx.theme.custtheme", {
	title : "custtheme",

	meta : {
		color : qx.theme.custtheme.Color,
		decoration : qx.theme.custtheme.Decoration,
		font : qx.theme.custtheme.Font,
		appearance : qx.theme.custtheme.Appearance,
		icon : qx.theme.icon.Tango
	}
});
